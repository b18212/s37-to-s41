//! [SECTION] dependencies
const express = require("express");
//http method
const router = express.Router();

//![SECTION] Imported Modules
const userControllers = require("../controllers/userControllers");
const auth = require("../auth");
//object destructuring from auth module

//* object destructuring from auth module
//so we can use it easily as middleware. Middleware for the routes
//when destructuring get property name, because function is their property name
const { verify, verifyAdmin } = auth;

//! [SECTION] routes

//*register user
router.post("/", userControllers.registerUser);

//? Mini Activity
// endpoint: '/'
// Create a route for getting all users.
// Use the appropriate HTTP method

//*get all users
router.get("/", userControllers.getAllUsers);

// --------

//*login user
router.post("/login", userControllers.loginUser);

//* get user details / profile view
//user details(get) auth; use verify for the token and controller connection -middleware
router.get("/getUserDetails", verify, userControllers.getUserDetails);

//* Check Email if Exists
router.post("/checkEmail", userControllers.checkEmail);

//* Find user by Email
router.post("/findUserByEmail", userControllers.findUserByEmail);

//* Find user Details
router.put("/updateUserDetails", verify, userControllers.updateUserDetails);

//? Mini Activity
// Create a user route which is able to capture the id from its url using route params
// This route will only update a regular user to an admin
// Only an admin can access this route
// Test the route in postman

//* Update a regular user to an admin
router.put(
    "/updateUserAdmin/:id",
    verify,
    verifyAdmin,
    userControllers.updateAdmin
);

//*Enrollment
//no need to put verifyAdmin
router.post("/enroll", verify, userControllers.enroll);

//*getting enrollment
router.get("/getEnrollments", verify, userControllers.getEnrollments);
// ------
module.exports = router;