//! [SECTION] Dependencies
const express = require("express");
const router = express.Router();

//![SECTION] Imported Modules
const courseControllers = require("../controllers/courseControllers");
//add Course
const auth = require("../auth");

// it will apply the functions verify, verifyadmin from auth.js.
const { verify, verifyAdmin } = auth;

//* create/add course
//middleware verify process when you are logged in
//verifyAdmin, logged in; if feature is for admin
router.post("/", verify, verifyAdmin, courseControllers.addCourse);

//* get all courses
router.get("/", courseControllers.getAllCourses);

//* get single course
//use id to locate the a single course you want to update
router.get("/getSingleCourse/:id", courseControllers.getSingleCourse);

//* update Course
router.put("/:id", verify, verifyAdmin, courseControllers.updateCourse);

//* Archive Course
//verify - function from Auth.js
router.put(
    "/archive/:id",
    verify,
    verifyAdmin,
    courseControllers.archiveCourse
);

//* Activate Course
router.put(
    "/activate/:id",
    verify,
    verifyAdmin,
    courseControllers.activateCourse
);

//* get all activate courses
router.get("/getActiveCourses", courseControllers.getAllActiveCourses);

//*find courses by name
router.post("/findCoursesByName", courseControllers.findCoursesByName);

module.exports = router;