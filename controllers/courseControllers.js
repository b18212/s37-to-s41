//! [SECTION] Dependencies
const Course = require("../models/Course");

//! [SECTION] Add Course
module.exports.addCourse = (req, res) => {
    console.log(req.body);

    let newCourse = new Course({
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
    });

    newCourse
        .save()
        .then((course) => res.send(course))
        .catch((err) => res.send(err));
};

//! [SECTION] Get All Courses

//find all documents in the Course collection
//We will use the find() method of our Course model
//Because the Course model is connected to our courses collection

module.exports.getAllCourses = (req, res) => {
    Course.find({})
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
};

//?[ACTIVITY 3]
//! [SECTION] Get Single Courses
//console.log(req.params.id)
//Check the if you can receive the data coming from the url.
//let id = req.params.id

module.exports.getSingleCourse = (req, res) => {
    Course.findById(req.params.id)
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
};

//![SECTION] Updating a Course
// when you are updating table , make sure to initialize a new variable where you can store the new properties, values for the table/collection you want to update
module.exports.updateCourse = (req, res) => {
    let updates = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
    };

    //*id user is the one updating use req.user.id
    Course.findByIdAndUpdate(req.params.id, updates, { new: true })
        .then((updatedCourse) => res.send(updatedCourse))
        .catch((err) => res.send(err));
};

//? [ACTIVITY 4]
//![SECTION] for archive inactivate Course
module.exports.archiveCourse = (req, res) => {
    let updates = {
        isActive: false,
    };

    Course.findByIdAndUpdate(req.params.id, updates, { new: true })
        .then((archivedCourse) => res.send(archivedCourse))
        .catch((err) => res.send(err));
};

//![SECTION] for activate Cource
module.exports.activateCourse = (req, res) => {
    let activate = {
        isActive: true,
    };

    Course.findByIdAndUpdate(req.params.id, activate, { new: true })
        .then((activatedCourse) => res.send(activatedCourse))
        .catch((err) => res.send(err));
};

//![SECTION] for get all active courses
module.exports.getAllActiveCourses = (req, res) => {
    Course.find({ isActive: true })
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
};

//![SECTION] Find courses by name

module.exports.findCoursesByName = (req, res) => {
    console.log(req.body); // contain the name of the course you are looking for

    //find a name / find method check all the document
    //$option: '$i' - to remove the case sensitive
    Course.find({ name: { $regex: req.body.name, $options: "$i" } }).then(
        (result) => {
            //if didn't find the name
            if (result.length === 0) {
                return res.send("No courses found");
            } else {
                return res.send(result);
            }
        }
    );
};