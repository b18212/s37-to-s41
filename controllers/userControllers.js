//! [SECTION] dependencies
//relative path, model creation
const User = require("../models/User");
//hashing of password
const Course = require("../models/Course"); //
const bcrypt = require("bcryptjs");
//authentication
const auth = require("../auth");

//! [SECTION] Register User
module.exports.registerUser = (req, res) => {
    console.log(req.body); //assurance where you will get the data

    //* password hashing, use hashSync method
    /*
		bcrypt- add a layer of security to your user's password
		What bcrypt does is hash our password into a randomized character version of the original string
		bcrypt.hashSync(<stringToBeHashed>, <saltRounds>)
		salt rounds- number of times the characters in the hash randomized
		let password = req.body.password
	*/

    const hashedPW = bcrypt.hashSync(req.body.password, 10);

    //* Create a new user document out of our user model
    let newUser = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        mobileNo: req.body.mobileNo,
        password: hashedPW,
    });

    newUser
        .save()
        .then((user) => res.send(user))
        .catch((err) => res.send(err));
};

//! [SECTION] Retrieval of all users

module.exports.getAllUsers = (req, res) => {
    User.find({})
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
};

//! [SECTION] Login User

module.exports.loginUser = (req, res) => {
    console.log(req.body);
    /*
		1. find user by email
		2. if user found check the password
		3. if user cannot be found, send message to the client
		4. if found upon checking and pass is the same, generate the access token. If not, reject by sending a message to the client
		*/

    User.findOne({ email: req.body.email })
        .then((foundUser) => {
            if (foundUser === null) {
                return res.send("No user found in the database.");
            } else {
                //bcrypt method, compareSync. uses 2 value that you want to compare. Boolean result
                const isPasswordCorrect = bcrypt.compareSync(
                    req.body.password,
                    foundUser.password
                );
                console.log(isPasswordCorrect);

                //* compareSync()
                // will return a boolean value so if it matches, this will return true if not, this will return false

                //no need comparison because of truthy values
                if (isPasswordCorrect) {
                    return res.send({ accessToken: auth.createAccessToken(foundUser) });
                } else {
                    return res.send("Incorrect password please try again.");
                }
            }
        })
        .catch((err) => res.send(err));
};

//! [SECTION] Getting Single User Details
module.exports.getUserDetails = (req, res) => {
    //auth js
    console.log(req.user);

    /*
	expected output: decoded token
		{
		  id: '6296fea12e45745b86178021',
		  email: 'callMeLisa@gmail.com',
		  isAdmin: false,
		  iat: 1654136799
		}

	*/

    //find the login users document from our database and send it to client by its id
    //decoded token have id: email: isAdmin:
    User.findById(req.user.id)
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
};

//! [SECTION] Check if Email Exists
module.exports.checkEmail = (req, res) => {
    console.log(req.body); // check if you can receive the email from our client's request body.

    //You can use find({email: req.body.email}), however, find() returns multiple documents in an ARRAY.
    User.findOne({ email: req.body.email })
        .then((foundEmail) => {
            if (foundEmail === null) {
                return res.send("Email is available.");
            } else {
                return res.send("Email is already registered.");
            }
        })
        .catch((err) => res.send(err));
};

// module.exports.findUserByEmail = (req, res) => {
// 	User.findOne({email: req.body.email}).then(result => res.send(result)).catch(err => res.send(err))
// };

//! [SECTION] Find User By Email
module.exports.findUserByEmail = (req, res) => {
    User.findOne({ email: req.body.email })
        .then((result) => {
            if (result === null) {
                return res.send("Cannot find user");
            } else {
                return res.send(result);
            }
        })
        .catch((err) => res.send(err));
};

//! [SECTION] Updating User Details

module.exports.updateUserDetails = (req, res) => {
    console.log(req.body); // input for new values
    console.log(req.user.id); //if user is already logged in
    let updates = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        mobileNo: req.body.mobileNo,
    };

    //new:true reflect the updated data
    //where to get updates, id, options
    //findById so you will not go through params
    User.findByIdAndUpdate(req.user.id, updates, { new: true })
        .then((updatedUser) => res.send(updatedUser))
        .catch((err) => res.send(err));
};

//![SECTION] Update admin

module.exports.updateAdmin = (req, res) => {
    console.log(req.user.id); //id of the logged in user
    console.log(req.params.id); //this will be the id of the user we want to update

    let updates = {
        isAdmin: true,
    };

    //! inside where to update
    User.findByIdAndUpdate(req.params.id, updates, { new: true })
        .then((updatedUser) => res.send(updatedUser))
        .catch((err) => res.send(err));
};

//! [SECTION] ENROLLMENT
//async and await (partners) - it makes the codes process or run at the same time. To load the website faster/ efficiency
//async - makes the function asynchronous (wait for the result of the function)
// unlike the javascript it reads line by line from top to buttom once it was detected the error it will stop
module.exports.enroll = async(req, res) => {
    /*
Steps:
1. Look for the USER by its id
    -- once we find the user push the details of the course we're trying to enroll in.
    We'll push to a new subdocument in our user

2. Look for the COURSE by its id
    Remember: sepertate User and Course
    -- pushg the details of the user/enrollee who's trying to enroll. We'll push to a new enrollees subdocument in our course

3. We both saving of documents are successful, we send a message to a client
        (once embeded in different collections)


                                                        */
    console.log(req.user.id); // look for the user by it's ID in the LOG IN USER and get the token
    // the user's id from the decoded token after verify
    console.log(req.body.courseId); //the course from our request body
    console.log(req);

    /*
    USER
    firstName - string,
    lastName - string,
    email - string,
    password - string,
    mobileNo - string,
    isAdmin -  boolean,
        default: false
        enrollment: [
        {
            courseId: string,
            status: string,
            dateEnrolled: date
        }
    ]
]
    */

    //req.user - it's coming from the request module
    //user - decoded token

    if (req.user.isAdmin) {
        //if ADMIN for example wanted to shop on his own Website it should be forbidden as it only allows on the reg user
        return res.send("Action Forbidden");
    }

    //await - wait to complete to update the user (embed) enroll
    let isUserUpdated = await User.findById(req.user.id).then((user) => {
        console.log(user);

        /*
Example
        {
            id: 48g4sa54ds2d1s,
            firstName: <userValue>
            lastName: <userValue>
            email: <userValue>
            password: <userValue>
            mobileNo: <userValue>
            isAdmin: <userValue>
                enrollments: [{<userValue}]  (embedd this)
                {
                    courseId: string, (Required)
                    status: string, (default)
                    dateEnrolled: date (default)
                }
        }

                                            */

        let newEnrollment = {
            courseId: req.body.courseId, //(coz courseId is required)
        };

        //user enrollment will push
        // the one who log in he will be the only one can enroll
        user.enrollments.push(newEnrollment);
        //(See the example)
        return user
            .save() //once saved
            .then((user) => true) //if the save properly it will return true //if not true or doen't save successfully it will stop
            .catch((err) => err.message); //else it will show error
    });
    if (isUserUpdated !== true) {
        //if error or false once it wasn't successfully save or discrepancies or failure on this part
        //sometimes it doesn't give us an error by using this we can identify where would be the error
        return res.send({ message: isUserUpdated });
    }

    //those who log in it will become an enrollee
    // we the course that user's wanted to enroll
    let isCourseUpdated = await Course.findById(req.body.courseId).then(
        (course) => {
            let enrollee = {
                userId: req.user.id,
            };

            course.enrollees.push(enrollee);

            return course
                .save()
                .then((course) => true)
                .catch((err) => err.message);
        }
    );

    if (isCourseUpdated !== true) {
        //false
        return res.send({ message: isCourseUpdated });
    }

    if (isUserUpdated && isCourseUpdated) {
        //if true && true it will suceessfully enrolled
        return res.send({ message: "User enrolled successfully" });
    }
};

//! [SECTION] Get Enrollments
//we need to find out who getting enrollment
module.exports.getEnrollments = (req, res) => {
    //result  (all details)
    User.findById(req.user.id) // to check if log in
        .then((result) => res.send(result.enrollments))
        .catch((err) => res.send(err));
};