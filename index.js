//! [SECTION] Dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

//! [SECTION] server
const app = express();

//! port
const port = 4000;

//! Database Connection
mongoose.connect(
    "mongodb+srv://admin:admin@wdc028-course-booking.jck1e.mongodb.net/Booking?retryWrites=true&w=majority", {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    }
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("Successfully connected to MongoDB"));

//! [SECTION] Middlewares - use method enables us to use express methods
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

//! [SECTION] Group routing section
const userRoutes = require("./routes/userRoutes");
app.use("/users", userRoutes);

const courseRoutes = require("./routes/courseRoutes");
app.use("/courses", courseRoutes);

//! port listener
app.listen(port, () => console.log(`Server is running at port ${port}.`));