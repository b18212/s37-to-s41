//![SECTION] DEPENDENCIES
const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI"; // like a signature of token
// once declared it cannot be change

//*Notes

//* JWT is a way to securely pass information from one part of a server to the frontend or other parts of our application. This will allow us to authorize our users to access or disallow acces to certain parts of our app.

//*JWT is like a gift wrapping service that is able to encode our user details which can only be unwrapped by jwt's own methods and if the secret provided is intact.
//* IF the JWT seemed tampered with, we will reject the users attempt to access a feature

module.exports.createAccessToken = (user) => {
    console.log(user);

    //* data object ius created to contain some details of the user
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin,
    };
    console.log(data);

    //sign has 3 arguments
    return jwt.sign(data, secret, {});
};

//! NOTES:

//* 1. You can only get access token when a user logs in in your app with the correct credentials

//* 2. As a user, you can only get your own details from your own token from logging in

//* 3. JWT is not meant for sensitive data.

//* 4. JWT is like a passport, you use around the app to access certain features meant for your type of user

//? Goal: to check if the token exist is not tampered
//*verify - for access token
module.exports.verify = (req, res, next) => {
    //req.headers.authorization - contains jwt/ token
    let token = req.headers.authorization;

    if (typeof token === "undefined") {
        //* typeof result is a string

        return res.send({ auth: "Failed. No Token" });
    } else {
        console.log(token);
        //* slice method is used for arrays and strings
        // Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyOTZmZWExMmU0NTc0NWI4NjE3ODAyMSIsImVtYWlsIjoiY2FsbE1lTGlzYUBnbWFpbC5jb20iLCJpc0FkbWluIjpmYWxzZSwiaWF0IjoxNjU0MTMxODQyfQ.RVmGn5gzvC1LChXKomeXSsiW0rFqZNDQSmEmvN2_C4o

        token = token.slice(7, token.length);
        console.log(token);
        //* Expected Output
        // eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyOTZmZWExMmU0NTc0NWI4NjE3ODAyMSIsImVtYWlsIjoiY2FsbE1lTGlzYUBnbWFpbC5jb20iLCJpc0FkbWluIjpmYWxzZSwiaWF0IjoxNjU0MTMxODQyfQ.RVmGn5gzvC1LChXKomeXSsiW0rFqZNDQSmEmvN2_C4o

        jwt.verify(token, secret, (err, decodedToken) => {
            if (err) {
                return res.send({
                    auth: "Failed",
                    message: err.message,
                });
            } else {
                console.log(decodedToken);

                req.user = decodedToken;

                //* will let us proceed to the next middleware or controller
                next();
            }
        });
    }
};

//! verifying an admin

module.exports.verifyAdmin = (req, res, next) => {
    if (req.user.isAdmin) {
        next();
    } else {
        return res.send({
            auth: "Failed",
            message: "Action Forbidden",
        });
    }
};